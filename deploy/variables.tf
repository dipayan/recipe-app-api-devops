variable "prefix" {
  type        = string
  default     = "raad"
  description = "Prefix for our recipe application"
}


variable "project" {
  type        = string
  default     = "recipe-app-api-devops"
  description = "Prefix for our recipe application"
}

variable "contact" {
  type        = string
  default     = "me@dipayanb.com"
  description = "Contact information of the maintainer"
}


variable "db_username" {
  description = "Username for RDS postgres instance"
  type        = string
}

variable "db_password" {
  description = "Password for RDS postgres instance"
  type        = string
}